#!/usr/bin/env luajit

local APT = require 'apt-panopticommon'
local D = APT.D
local I = APT.I
local T = APT.T
local W = APT.W
local E = APT.E
local C = APT.C
local arg, sendArgs = APT.parseArgs({...})


APT.mirrors = loadfile("results/mirrors.lua")()
APT.html = false
for k, v in APT.orderedPairs(APT.mirrors) do
    APT.doRRD('results', k, v)
end

APT.debians = loadfile("results/debians.lua")()
APT.html = false
for k, v in APT.orderedPairs(APT.debians) do
    APT.doRRD('results', k, v)
end
